# platform-gen

#### 项目介绍
大平台代码生成工具IDEA插件

#### 使用教程

1. doc/IDEA插件-代码生成工具使用.pdf

## 官网
[http://fly2you.cn](http://fly2you.cn)

## 后台管理系统演示
* 演示地址：[http://fly2you.cn/platform](http://fly2you.cn/platform)  `账号密码：admin/admin`

* 如何交流、反馈、参与贡献？
    * 官方QQ群：<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=75689ba2797dd88a208446088b029fbdeba87a29315ff2a021a6731f22ef5052"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="大平台系统开发" title="大平台系统开发"></a>
    * 博客：[http://fly2you.cn/mblog](http://fly2you.cn/mblog)
    * git：[https://gitee.com/fuyang_lipengjun/platform](https://gitee.com/fuyang_lipengjun/platform)
    * 基础架构版
        * git：[https://gitee.com/fuyang_lipengjun/platform-framework](https://gitee.com/fuyang_lipengjun/platform-framework)
        * 演示地址：[http://fly2you.cn/platform-framework](http://fly2you.cn/platform-framework)
    * 如需获取项目最新源码，请Watch、Star项目，同时也是对项目最好的支持

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request